# kmeans-php

A solution developed with PHP for "free text" clustering, in natural human language, using "Bag of Words" method for text mining with k-means algorithm.

# purpose
Textual analysis software in natural human language using records from a Help Desk team in order to group these tickets and distribute them to the attendants in a way that the list of tasks of each attendant is more homogeneous, thus contributing to their higher productivity.

# Documents
## use-case diagram
![use-case diagram](doc/use-cases.jpg)

## class diagram
![class diagram](doc/class-diagram.jpg)

### train
![use-case diagram](doc/treinar-activity-diagram.jpg)

### execute
![use-case diagram](doc/executar-activity-diagram.jpg)