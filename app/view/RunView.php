﻿<?php
    $stopwords = ["de", "a", "o", "que", "e", "do", "da", "em", "um", "para", "e", "com", "nao", "uma", "os", "no", "se", "na", "por", "mais", "as", "dos", "como", "mas", "foi", "ao", "ele", "das", "tem", "a", "seu", "sua", "ou", "ser", "quando", "muito", "ha", "nos", "ja", "esta", "eu", "tambem", "so", "pelo", "pela", "ate", "isso", "ela", "entre", "era ", "depois", "sem", "mesmo", "aos", "ter ", "seus", "quem", "nas", "me", "esse", "eles", "estao ", "voce", "tinha ", "foram ", "essa", "num", "nem", "suas", "meu", "as", "minha", "tem ", "numa", "pelos", "elas", "havia ", "seja ", "qual", "sera ", "nos", "tenho ", "lhe", "deles", "essas", "esses", "pelas", "este", "fosse ", "dele", "tu", "te", "voces", "vos", "lhes", "meus", "minhas", "teu", "tua", "teus", "tuas", "nosso", "nossa", "nossos", "nossas", "dela", "delas", "esta", "estes", "estas", "aquele", "aquela", "aqueles", "aquelas", "isto", "aquilo", "estou", "esta", "estamos", "estao", "estive", "esteve", "estivemos", "estiveram", "estava", "estavamos", "estavam", "estivera", "estiveramos", "esteja", "estejamos", "estejam", "estivesse", "estivessemos", "estivessem", "estiver", "estivermos", "estiverem", "hei", "ha", "havemos", "hao", "houve", "houvemos", "houveram", "houvera", "houveramos", "haja", "hajamos", "hajam", "houvesse", "houvessemos", "houvessem", "houver", "houvermos", "houverem", "houverei", "houvera", "houveremos", "houverao", "houveria", "houveriamos", "houveriam", "sou", "somos", "sao", "era", "eramos", "eram", "fui", "foi", "fomos", "foram", "fora", "foramos", "seja", "sejamos", "sejam", "fosse", "fossemos", "fossem", "for", "formos", "forem", "serei", "sera", "seremos", "serao", "seria", "seriamos", "seriam", "tenho", "tem", "temos", "tem", "tinha", "tinhamos", "tinham", "tive", "teve", "tivemos", "tiveram", "tivera", "tiveramos", "tenha", "tenhamos", "tenham", "tivesse", "tivessemos", "tivessem", "tiver", "tivermos", "tiverem", "terei", "tera", "teremos", "terao", "teria", "teriamos", "teriam", "gentileza", "prezados", "priorizar", "avaliar", "tarde", "noite", "dia", "bom", "boa", "favor", "abaixo", "acima", "tarde", "noite", "dia"];
?>
<!DOCTYPE html>
<html>
<head>
    <title>TCC</title>
    <meta charset="utf-8" >
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>
<body>
<h1>Textmining usando K-means</h1>
<h4>Insira o arquivo cujos dados deseja agrupar</h4>
<div class="container">
    <form method="post" action="controller/indexController.php" enctype="multipart/form-data">
        <div class="form-group row">
            <label for="k" class="col-sm-1 col-form-label">Grupos:</label>
            <div class="col-sm-2">
                <input id="k" name="k" type="number" class="form-control" placeholder="0">
            </div>
            <label for="iteract" class="col-sm-1 col-form-label">Repetiçoes:</label>
            <div class="col-sm-2">
                <input id="iteract" name="iteract" type="number" class="form-control" placeholder="0">
            </div>
        </div>
        <div class="form-group">
            <input type="file" name="datasource" class="file">
            <div class="input-group col-xs-12">
                <span class="input-group-btn">
                    <button class="browse btn btn-primary input-lg" type="button">
                        <i class="glyphicon glyphicon-search"></i>Procurar</button>
                </span>
                <input type="text" class="form-control input-lg" disabled placeholder="Insira um arquivo">
            </div>
        </div>
        <div class="form-group">
            <h3 class="form-title">Tratamentos no texto:</h3>
            <div class="form-check form-check-inline">
                <input checked class="form-check-input" type="checkbox" name="rmvmaiusculas" value="1" id="rmvmaiusculas">
                <label class="form-check-label" for="rmvmaiusculas">mai&uacute;sculas</label>
            </div>
            <div class="form-check form-check-inline">
                <input checked class="form-check-input" type="checkbox" name="rmvstopwords" value="1" id="rmvstopwords">
                <label class="form-check-label" for="rmvstopwords">stop words</label>
            </div>
            <div class="form-check form-check-inline">
                <input checked class="form-check-input" type="checkbox" name="rmvdatas" value="1" id="rmvdatas">
                <label class="form-check-label" for="rmvdatas">datas </label>
            </div>
            <div class="form-check form-check-inline">
                <input checked class="form-check-input" type="checkbox" name="rmvhorario" value="1" id="rmvhorario">
                <label class="form-check-label" for="rmvhorario">hor&aacute;rios</label>
            </div>
            <div class="form-check form-check-inline">
                <input checked class="form-check-input" type="checkbox" name="rmvspecialchars" value="1" id="rmvspecialchars">
                <label class="form-check-label" for="rmvspecialchars">caracteres especiais</label>
            </div>
            <div class="form-check form-check-inline">
                <input checked class="form-check-input" type="checkbox" name="rmvnumeros" value="1" id="rmvnumeros">
                <label class="form-check-label" for="rmvnumeros">n&uacute;meros</label>
            </div>
            <div class="form-check form-check-inline">
                <input checked class="form-check-input" type="checkbox" name="rmvsingleletters" value="1" id="rmvsingleletters">
                <label class="form-check-label" for="rmvsingleletters">letras soltas</label>
            </div>
            <div class="form-check form-check-inline">
                <input checked class="form-check-input" type="checkbox" name="rmvspaces" value="1" id="rmvspaces">
                <label class="form-check-label" for="rmvspaces">espa&ccedil;os multiplos</label>
            </div>
        </div>
        <div class="form-group">
            <label for="stopwords"><h3>Stop words:</h3></label>
            <textarea required name="stopwords" class="form-control" id="stopwords" rows="5"><?=trim(implode(", ",$stopwords));?></textarea>
        </div>
        <div class="form-group text-center">
            <input type="submit" class="btn btn-success" value="enviar">
        </div>
    </form>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="script.js"></script>
</body>
</html>