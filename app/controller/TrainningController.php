﻿<?php

//tratmento utf-8
setlocale(LC_ALL, 'pt_BR.utf8');
ini_set('charset', 'utf8');
header("Content-Type: text/html; charset=ISO-8859-1", true);
require_once('../model/Plan.php');
require_once('../model/Item.php');

//Definindo timezone
date_default_timezone_set('America/Sao_Paulo');


//definindo nivel de log de erros
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('track_errors', 1);

//definindo timeout config
set_time_limit(6000);


//filtra opcoes de execucao
$k = filter_input(INPUT_POST, 'k', FILTER_SANITIZE_NUMBER_INT);
$iteract = filter_input(INPUT_POST, 'iteract', FILTER_SANITIZE_NUMBER_INT);

if (!isset($k)) {
    $k = 10;
}
if (!isset($iteract)) {
    $iteract = 30;
}

//filtra opcoes de tratamento
$rmvmaiusculas = filter_input(INPUT_POST, 'rmvmaiusculas', FILTER_DEFAULT);
$rmvstopwords = filter_input(INPUT_POST, 'rmvstopwords', FILTER_DEFAULT);
$rmvdatas  = filter_input(INPUT_POST, 'rmvdatas', FILTER_DEFAULT);
$rmvhorario = filter_input(INPUT_POST, 'rmvhorario', FILTER_DEFAULT);
$rmvspecialchars = filter_input(INPUT_POST, 'rmvspecialchars', FILTER_DEFAULT);
$rmvnumeros = filter_input(INPUT_POST, 'rmvnumeros', FILTER_DEFAULT);
$rmvsingleletters = filter_input(INPUT_POST, 'rmvsingleletters', FILTER_DEFAULT);
$rmvspaces = filter_input(INPUT_POST, 'rmvspaces', FILTER_DEFAULT);

$stopwords = explode(', ', trim(filter_input(INPUT_POST, 'stopwords', FILTER_SANITIZE_STRING)));

//ciração do plano de execução com quantidade de centroids e iterações
$plan = new Plan($k, $iteract, $stopwords);

//die(var_dump(file_get_contents($_FILES['datasource']['tmp_name'])));
//Captura arquivo
$dados = explode("\n", file_get_contents($_FILES['datasource']['tmp_name']));
//die(var_dump($dados));
//importa lib bootstrap
echo '<html><head><title>Resultado</title><meta charset="utf-8" ><meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"></head>
<body><div class="container">';

$cabecalho = array_shift($dados);

foreach ($dados as $linha) {
    $dadosLinha = explode(";", $linha);

    $item = new Item();
    $item->setId(array_shift($dadosLinha));
    $item->setText(implode(" ", $dadosLinha));

    $item->extractWordsFromText();

    //Trata linhas vazias no arquivo
    if (is_null($item->getWords()) || count($item->getWords()) ==0) {
        unset($item);
        continue;
    }


    if ($rmvmaiusculas) {
        $item->toLower();
    }

    if ($rmvstopwords) {
        $item->removeStopwords($plan->getStopwords());
    }
    if ($rmvdatas) {
        $item->noDates();
    }
    if ($rmvhorario) {
        $item->noTime();
    }
    if ($rmvspecialchars) {
        $item->noDecorate();
    }
    if ($rmvnumeros) {
        $item->noNumbers();
    }
    if ($rmvsingleletters) {
        $item->noSingleLetter();
    }
    if ($rmvspaces) {
        $item->noEmpty();
    }

    if ($rmvstopwords) {
        $item->removeStopwords($plan->getStopwords());
    }
    if ($rmvspecialchars) {
        $item->noDecorate();
    }
    if ($rmvnumeros) {
        $item->noNumbers();
    }
    if ($rmvsingleletters) {
        $item->noSingleLetter();
    }
    if ($rmvspaces) {
        $item->noEmpty();
    }

    $item->updateDims();

    $plan->pushItem($item);
    $plan->pushDim($item->getWords());
}

//$plan->startRandomCentroids();
$plan::$log .= $plan->startCopyCentroids();
//$plan->showItensFitTable();

echo  "<br><br><br>Itens apurados: " . count($plan->getItens()) . "<br><br><br>";

for ($i=0; $i < $plan->getIteract(); $i++) {
    echo "<br><h1>Itera&ccedil;&atilde;o $i</h1>";
    $plan::$log .= "Itera&ccedil;&atilde;o $i";

    $plan->showCentroidsFitTable();
    $plan->linkItens();
    $plan::$log .= $plan->reallocCentroids();
}


$plan->showResult();

//import js and close html
echo '</div><script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script><script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script></body></html>';

