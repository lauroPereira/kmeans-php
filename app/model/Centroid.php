﻿<?php

require_once('Item.php');
class Centroid extends Item{
	
	private $id;
	private $words;

	private $itens;
	private $dims;

	function __construct($id){
		$this->id = $id;
		$this->words = array();
		$this->itens = new SplObjectStorage();
		$this->dims = array();
	}

	public function pushWord($word, $quantity){
		for ($i=0; $i < $quantity; $i++) { 
			array_push($this->words, $word);
		}
	}

    public function setWords($words){
        $this->words = $words;
    }

	public function getDims(){
		return $this->dims;
	}

	/**
     * @var $key string
     * @var $vlr float
     * @return string
     */
	public function setDim($key, $vlr){
	    $log = '';
		$dimVlr = (!isset($this->dims[$key]))?0:$this->dims[$key];
		if($dimVlr != $vlr){
			$log .= '<br/>&nbsp;&nbsp;&nbsp;&nbsp;- '."dimensao #C{$this->getId()}[{$key}] tinha {$dimVlr} e agora tem ".(float)$vlr;
            	$this->dims[$key] = $vlr;
        }
        return $log;
	}

	public function updateDims(){
		$this->dims = array_count_values($this->getWords());
	}

	public function getWords(){
		return $this->words;
	}

	public function getId(){
		return $this->id;
	}

	public function pushItem(Item $item){
		$this->itens->attach($item);
	}

    public function popItem(Item $item){

	    $this->itens->rewind();
        while($this->itens->valid()) {
            if($item->getId() == $this->itens->current()->getId()){
                $this->itens->detach($this->itens->current());
            }else{
                $this->itens->next();
            }
        }
    }

    /**@return SplObjectStorage*/

	public function getItens(){
		return $this->itens;
	}

    /**@return array*/
	public function showItens(){
		$arr = array();
		
		$this->itens->rewind();
		while($this->itens->valid()) {

			array_push($arr, $this->itens->current()->getId());
			$this->itens->next();
		}
		
		return $arr;
	}
}
?>