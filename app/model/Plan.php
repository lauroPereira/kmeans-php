﻿<?php

require_once('../model/Centroid.php');

class Plan {

	private $dim;
	private $k;
	private $iteract;
    private $stopwords = array();

    public static $itens = array();
    public static $centroids = array();

	public static $instance;

	public static $log = '';

	/**
     * @var $k integer
     * @var $iteract integer
     * @var $stopwords array
     * @return Plan
     */
	public function __construct($k, $iteract, $stopwords) {
        $this->dim = array();
        $this->k = $k;
        $this->iteract = $iteract;
        $this->stopwords = $stopwords;

        self::$instance = $this;
    }

    /**
     * Método para obter a instancia do plano de execução
     *
     * @return Plan
     * @var $k integer
     * @var $stopwords array
     * @var $iteract integer
     */
    public static function get($k, $iteract, $stopwords) {
        if (self::$instance === null) {
            self::$instance = new self($k, $iteract, $stopwords);
        }
        return self::$instance;
    }

    public function pushItem($item){
        array_push(self::$itens, $item);
    }

    public function pushDim($words){
        /* @var $word string*/
        foreach ($words as $word) {
            if(!in_array($word, $this->dim)){
                array_push($this->dim, $word);
            }
        }
    }

    public function getIteract(){
        return $this->iteract;
    }

    public function getDim(){
        return $this->dim;
    }

    public function getItens(){
        return self::$itens;
    }

    public function getCentroids() {
        return self::$centroids;
    }

    public function startRandomCentroids(){
        for ($i=0; $i < $this->k; $i++) { 
            $centroid = new Centroid($i);

            /* @var $dim string */
            foreach ($this->dim as $dim) {
                $countWord = rand(0, 10000);
                if($countWord > 9999){
                    $centroid->pushWord($dim, 3);
                }else if($countWord > 9997){
                    $centroid->pushWord($dim, 2);
                }else if($countWord > 9930){
                    $centroid->pushWord($dim, 1);
                }
            }
            $centroid->updateDims();
            array_push(self::$centroids, $centroid);
        }
        return self::$centroids;
    }

    public function startCopyCentroids(){
        $log = '';
        for ($i=0; $i < $this->k; $i++) {
            /** @var $centroid Centroid*/
            $centroid = new Centroid($i);

            $id = rand(0,count(self::$itens)-1);

            /** @var $shadow_item Item*/
            $shadow_item = self::$itens[$id];
            $log = "O centroid #{$centroid->getId()} foi criado com base no item {$shadow_item->getId()}.";

            $centroid->setWords($shadow_item->getWords());
            $centroid->updateDims();

            array_push(self::$centroids, $centroid);
        }
        return $log;
    }

    public function getStopwords(){
        return $this->stopwords;
    }

    public function showItensTable(){
        echo '<h1>Itens:</h1>';
        echo '<table border=1>';
        echo '<thead><tr><th>id Item</th><th>Words</th><th>'
            .implode("</th><th>", $this->getDim())
            .'</th></tr></thead>';
        echo '<tbody>';
        /* @var $item Item */
        foreach (self::$itens as $item){
            echo '<tr><td>'.$item->getId().'</td>';
            echo '<td><ul><li>'
                 .implode("</li><li>", $item->getWords())
                .'</li></ul></td>';
            /* @var $dim string */
            foreach ($this->getDim() as $dim) {
                echo '<td>';
                $itemDims = $item->getDims();
                if(in_array($dim, array_keys($itemDims))){;
                    echo $itemDims[$dim];
                }else{
                    echo "0";
                }
                echo '</td>';
            }
            echo '</tr>';
        }
        echo '</tbody></table>';

    }

    public function showItensFitTable(){
        echo '<h1>Itens (Fit table):</h1>';
        echo '<table border=1>';
        echo '<thead><tr><th>id Item</th><th>Dimensions</th></tr></thead>';
        echo '<tbody>';
        /* @var $item Item */
        foreach (self::$itens as $item){
            echo '<tr><td>'.$item->getId().'</td>';
            echo '<td><ul>';
            /* @var $dim string */
            foreach($item->getDims() as $dim => $vlr) {
                echo "<li>{$dim}: {$vlr}</li>";
            }
            echo '</ul></td>';
            echo '</tr>';
        }
        echo '</tbody></table>';

    }

    public function showCentroidsFullTable(){

        echo '<h1>Centroids:</h1>';
        echo '<table border=1>';
        echo '<thead><tr><th>id Centroid</th><th>Words</th><th>Itens</th><th>'
            .implode("</th><th>", $this->getDim())
            .'</th></tr></thead>';
        echo '<tbody>';

        /* @var $centroid Centroid */
        foreach (self::$centroids as $centroid){
            echo '<tr><td>'.$centroid->getId().'</td>';
            echo '<td><ul><li>'
                 .implode("</li><li>", $centroid->getWords())
                .'</li></ul></td>';
            echo '<td><ul><li>'
                 .implode("</li><li>", $centroid->showItens())
                .'</li></ul></td>';

            /* @var $dim string*/
            foreach ($this->getDim() as $dim) {
                echo '<td>';
                $centroid->updateDims();
                $itemDims = $centroid->getDims();
                if(in_array($dim, array_keys($itemDims))){;
                    echo $itemDims[$dim];
                }else{
                    echo "0";
                }
                echo '</td>';
            }
            echo '</tr>';
        }
        echo '</tbody></table>';
    }

    public function showCentroidsFitTable(){

        echo '<h2>Centroids:</h2>';
        echo '<table border=1>';
        echo '<thead><tr><th>id Centroid</th><th>Words</th><th>Itens</th><th>Dimensions</th></tr></thead>';
        echo '<tbody>';
        /* @var $centroid Centroid*/
        foreach (self::$centroids as $centroid){
            echo '<tr><td>'.$centroid->getId().'</td>';
            echo '<td>'.implode(", ", $centroid->getWords()).'</td>';
            echo '<td><ul><li>'.count($centroid->showItens())
                .'</li></ul></td>';
            echo '<td><ul>';
            echo "<li>".count($centroid->getDims())."</li>";
            echo '</ul></td>';

            echo '</tr>';
        }
        echo '</tbody></table>';
    }

    public function showCentroidsTable(){

        echo '<h1>Centroids:</h1>';
        echo '<table border=1>';
        echo '<thead><tr><th>id Centroid</th><th>Words</th><th>Itens</th><th>Dimensions</th></tr></thead>';
        echo '<tbody>';
        /* @var $centroid Centroid*/
        foreach (self::$centroids as $centroid){
            echo '<tr><td>'.$centroid->getId().'</td>';
            echo '<td><ul><li>'
                .implode("</li><li>", $centroid->getWords())
                .'</li></ul></td>';
            echo '<td><ul><li>'.count($centroid->showItens())
                .'</li></ul></td>';
            echo '<td><ul>';

            /* @var $dim string */
            foreach($centroid->getDims() as $dim => $vlr) {
                echo "<li>{$dim}: {$vlr}</li>";
            }
            echo '</ul></td>';

            echo '</tr>';
        }
        echo '</tbody></table>';
    }

    public function showResult(){

        echo '<br><br><h1>Resultado da an&aacute;lise:</h1>';
        echo '<table border=1>';
        echo '<thead><tr><th>id Centroid</th><th>Itens</th><th>top words: </th><th>Itens: </th></tr></thead>';
        echo '<tbody>';
        /* @var $centroid Centroid*/
        foreach (self::$centroids as $centroid){
            $dims = $centroid->getDims();
            arsort($dims);
            echo '<tr>';
            echo "<td>{$centroid->getId()}</td>";
            echo "<td>{$centroid->getItens()->count()}</td>";
            echo '<td><ul>';
            foreach (array_slice($dims, 0, 5) as $idx => $vlr){
                echo "<li>{$idx}: {$vlr}</li>";
            }
            echo '</ul></td>';
            echo '<td><ul>';
            /** @var $item Item*/
            $itens_id = '';
            foreach ($centroid->getItens() as $item){
                $itens_id .= ', '.$item->getId();
                echo "<li>{$item->getId()}: {$item->getText()}</li>";
            }
            echo "<li><b>Itens consolidado: ".substr($itens_id, 2)."</b></li>";
            echo '</ul></td>';
        }
        echo '</tbody></table>';
    }

    public function linkItens(){

        $log = '';

        /* @var $item Item */
        foreach (self::$itens as $item){
            $firstCentroid = true;
            $dist = 0;
            $betterDist = 0;

            /* @var $centroid Centroid*/
            foreach (self::$centroids as $centroid){
                if($firstCentroid){
                    $firstCentroid = false;

                    //calcula distancia entre centroid e item
                    $dist = $this->dimAverage($item, $centroid);
                    
                    //Se o item ja esta em um centroid e retirado
                    $item->freeCentroid();

                    //item recebe novo vinculo do e centroid
                    $item->setCentroid($centroid);

                    //centroid tambem puxa referencia ao item
                    $centroid->pushItem($item);

                    //melhor distancia assume o valor da primeira distancia
                    $betterDist = $dist;
                }else{
                    //apenas para controle
                    $dist_old = $dist;

                    //calcula distancia entre centroid e item
                    $dist = $this->dimAverage($item, $centroid);

                    //caso nova distancia seja melhor do que a melhor
                    if($dist < $betterDist){

                        //Se o item ja esta em um centroid e retirado
                        $item->freeCentroid();

                        //item recebe novo vinculo do e centroid
                        $item->setCentroid($centroid);

                        //centroid tambem puxa referencia ao item
                        $centroid->pushItem($item);

                        //melhor distancia e atualizada
                        $betterDist = $dist;
                        $log .= "<br> Item {$item->getId()} recebeu o centroid {$centroid->getId()} (distancia antiga {$dist_old} e nova {$dist})";
                    }
                }
            }
        }
        return $log;
    }

    private function dimAverage(Item $itemA, Item $itemB){
        $val = 0;
        $pontoA = $itemA->getDims();
        $pontoB = $itemB->getDims();
        /* @var $word string*/
        foreach ($this->getDim() as $word) {
            $dimA = (!isset($pontoA[$word]))?0:$pontoA[$word];
            $dimB = (!isset($pontoB[$word]))?0:$pontoB[$word];
            $val += pow(($dimA - $dimB), 2);
        }
        $dist = sqrt($val);

        return $dist;
    }

    public function reallocCentroids(){

        $log = '';

        /* @var $centroid Centroid*/
        foreach ($this->getCentroids() as $centroid) {

            /* @var $word string*/
            foreach ($this->getDim() as $word) {
                
                //inicia variavel para calculo da media da dimensao
                /** @var $dimAverage float*/
                $dimAverage = 0;

                $log .= "<br>&nbsp;&nbsp;&nbsp;O centroid #{$centroid->getId()} possui ".count($centroid->getItens())." itens.";
                if($centroid->getItens()->count() > 0){

                    $centroid->getItens()->rewind();
                    while($centroid->getItens()->valid()) {
                        $dims = $centroid->getItens()->current()->getDims();
                        if(!isset($dims[$word])){
                            $dim = 0;
                        }else{
                            $dim = $dims[$word];
                        }

                        //soma o valor de cada item para essa dimencao
                        $dimAverage += $dim;

                        $centroid->getItens()->next();
                    }
                    //caso o centroid possua itens, divide a soma dos valores pela quantidade para calcular a media simples
                    $dimAverage = $dimAverage / $centroid->getItens()->count();
                    $centroid->setDim($word, $dimAverage);
                } else{

                    /** @var Centroid $full_centroid*/
                    $full_centroid = $this->getGreater();

                    $log .= "<br>&nbsp;&nbsp;&nbsp;O centroid #{$full_centroid->getId()} possui {$full_centroid->getItens()->count()} itens ";

                    $id = rand(0,$full_centroid->getItens()->count()-1);

                    $full_centroid->getItens()->rewind();
                    for($i=0;$i<$id;$i++) {
                        $full_centroid->getItens()->next();
                    }

                    /** @var Item $item*/
                    $item = $full_centroid->getItens()->current();

                    //Se o item ja esta em um centroid e retirado
                    $item->freeCentroid();

                    //item recebe novo vinculo do e centroid
                    $item->setCentroid($centroid);

                    //centroid tambem puxa referencia ao item
                    $centroid->pushItem($item);

                    $log .= "<br>&nbsp;&nbsp;&nbsp;O centroid #{$centroid->getId()} n&atilde;o possui itens ";
                    $log .= "e por isso pegou o item {$item->getId()} ";
                    $log .= "do centroid {$full_centroid->getId()}, que agora possui {$full_centroid->getItens()->count()} itens.";
                }
            }
        }
        return $log;
    }

    /**
     * @return Centroid
     */
    private function getGreater(){
        $count = 0;
        $result = null;
        /* @var $centroid Centroid*/
        foreach (self::$centroids as $centroid){
            if(count($centroid->getItens()) > $count){
                $count = count($centroid->getItens());
                $result = $centroid;
            }
        }

        return $result;
    }

    /**
     * Método clone do tipo privado previne a clonagem dessa instância
     * da classe
     *
     * @return void
     */
    private function __clone()
    {
    }

    /**
     * Método unserialize do tipo privado para prevenir a desserialização
     * da instância dessa classe.
     *
     * @return void
     */
    private function __wakeup(){}

}
?>