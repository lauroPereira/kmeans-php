﻿<?php

class Item {
	
	private $id;
	private $text;
	private $words;
	private $centroid;
	private $dims;

	function __construct(){
		$this->dims = array();
	}

	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getText(){
		return $this->text;
	}

	public function setText($text){
		$this->text = $text;
	}

	public function getWords(){
		return $this->words;
	}

	public function setWords($words){
        $this->words = $words;
    }

	/**@return Centroid*/
	public function getCentroid(){
		return $this->centroid;
	}

    /**@var $centroid Centroid*/
	public function setCentroid($centroid){
		$this->centroid = $centroid;
	}

    /**@return array*/
	public function getDims(){
		return $this->dims;
	}

    /**
     * @var $key string
     * @var $vlr integer
     * @return integer
     */
	public function setDim($key, $vlr){
		return $this->dims[$key] = $vlr;
	}

	public function updateDims(){
		$this->dims = array_count_values($this->getWords());
	}

	public function freeCentroid(){
		//se o item esta ligado a um centroid
		if(!is_null($this->getCentroid())){

            //e se a reciproca e verdadeira
            if($this->getCentroid()->getItens()->contains($this)){
				$this->getCentroid()->getItens()->detach($this);
			}
        }
	}

	public function extractWordsFromText(){
		if(isset($this->text) && $this->text != ''){
            $pattern = '/\s+/i';
			$replacement = ' ';
		
			$this->words = array_filter(explode(" ", preg_replace($pattern,$replacement,$this->text)), 'strlen');
		}
	}

	public function toLower(){
	    $arr = array();

		foreach ($this->words as $word) {
			array_push($arr, mb_strtolower($word));
		}

        unset($this->words);
        $this->words = $arr;
	}

	public function noDates(){

		foreach ($this->words as $key => $value) {
			$pattern = '/(\d+\/\d+(\/\d+)?)/i';
			$replacement = ' ';
			$this->words[$key] = preg_replace($pattern,$replacement,$value);
		}
	}
	
	public function noTime(){
		
		foreach ($this->words as $key => $value) {
			$pattern = '/(\d+\:\d+\:\d+)/i';
			$replacement = ' ';
			$this->words[$key] = preg_replace($pattern,$replacement,$value);
		}
	}

	public function noDecorate(){

		foreach ($this->words as $key => $value) {
			$pattern = array(
				"/(Â|á|à|ã|â|ä)/i",
				"/(é|è|ê|ë)/i",
				"/(í|ì|î|ï)/i",
				"/(ó|ó|ò|õ|ô|ö)/i",
				"/(ú|ù|û|ü)/i",
				"/(ñ)/i",
				"/(ç)/i",
				"/(\`|\´|\^|\~|\||\+|\¢|\£|\=|\@|\\|\/|\(|\{|\[|\)|\}|\]|\(|\)|\.|\,|\/|\;|\:|\?|\!|\*|\'|\"|\&|\%|\$|\#|\-|\_|\º|\ª)/i"
			);
			$replacement = explode(";","a;e;i;o;u;n;c; ");
			$this->words[$key] = preg_replace($pattern,$replacement,$value);
		}
	}
		
	public function noNumbers(){
		foreach ($this->words as $key => $value) {
			$pattern = '/\d+/i';
			$replacement = ' ';
			$this->words[$key] = preg_replace($pattern,$replacement,$value);
		}
	}

	public function noSingleLetter(){
		foreach ($this->words as $key => $value) {
			$pattern = '/\s[a-z]\s/i';
			$replacement = ' ';
			$this->words[$key] = preg_replace($pattern,$replacement,$value);
		}
	}

	public function noEmpty(){
		foreach ($this->words as $key => $value) {
			$pattern = '/\s?/i';
			$replacement = '';
			$this->words[$key] = preg_replace($pattern,$replacement,$value);
			if(!$this->words[$key] || $this->words[$key] == '' or empty($this->words[$key])){
				unset($this->words[$key]);
			}
		}
	}

	public function removeStopwords($stopwords){
		$this->words = array_diff($this->words, $stopwords);
	}

}
?>